# encoding:utf-8
"""
    Create on by Alan 2019-08-16 15:26
"""

from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True)
