# encoding:utf-8
"""
    Create on by Alan 2019-08-16 17:06
"""

import os

SECRET_KEY = os.urandom(24)
CSRF_ENABLED = True
