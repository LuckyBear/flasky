# encoding:utf-8
"""
    Create on by Alan 2019-08-16 15:55
"""

from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return '这是get请求'
    else:
        return '这是POST请求'


if __name__ == '__main__':
    app.run(debug=True)
